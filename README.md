INTRODUCTION
------------

This module integrate VNPAY payment gate way to Drupal Commerce.

  * VNPAY: https://vnpayment.vn/
  * Developers: http://sandbox.vnpayment.vn/apis/docs/

REQUIREMENTS
------------

This module requires the following modules:

 * Drupal Commerce (https://www.drupal.org/project/commerce)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * After install, add the payment method at:
 /admin/commerce/config/payment-gateways

MAINTAINERS
-----------

 Current maintainers:

 * Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
