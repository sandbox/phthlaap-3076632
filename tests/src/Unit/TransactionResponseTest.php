<?php

namespace Drupal\Tests\commerce_vnpay\Unit;

use Drupal\commerce_vnpay\VNPAY\GatewayArguments;
use Drupal\commerce_vnpay\VNPAY\TransactionResponse;
use Drupal\Tests\UnitTestCase;

/**
 * Class TransactionResponseTest.
 *
 * @package Drupal\Tests\commerce_vnpay\Unit
 */
class TransactionResponseTest extends UnitTestCase {
  protected $gatewayMessages;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->gatewayMessages = TransactionResponse::MESSAGES;
  }

  /**
   * Test response code binding from VNP parameters.
   */
  public function testGetResponseCodeAndMessages() {

    foreach ($this->gatewayMessages as $code => $message) {
      $response = new TransactionResponse([
        GatewayArguments::RESPONSE_CODE => $code,
      ]);

      $this->assertEquals($code, $response->getCode());
      $this->assertEquals($message, $response->getMessage());

    }

  }

}
