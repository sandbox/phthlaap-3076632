<?php

namespace Drupal\Tests\commerce_vnpay\Unit;

use Drupal\commerce_vnpay\VNPAY\PaymentGateway as PaymentGatewayAlias;

/**
 * Class PaymentGateway.
 *
 * @package Drupal\Tests\commerce_vnpay\Unit
 */
class PaymentGateway extends PaymentGatewayAlias {

  protected $fakeDate;

  protected $fakeIp;

  protected $fakeInfo;

  protected $fakeReturnUrl;

  /**
   * Set fake data.
   *
   * @param string $fakeDate
   *   Set fake data.
   */
  public function setFakeDate($fakeDate) {
    $this->fakeDate = $fakeDate;
  }

  /**
   * Set fake data.
   *
   * @param string $ip
   *   Fake data.
   */
  public function setFakeIp($ip) {
    $this->fakeIp = $ip;
  }

  /**
   * Set fake data.
   *
   * @param string $info
   *   Fake info.
   */
  public function setFakeInfo($info) {
    $this->fakeInfo = $info;
  }

  /**
   * Set fake return url.
   *
   * @param string $return_url
   *   Return url.
   */
  public function setFakeReturnUrl($return_url) {
    $this->fakeReturnUrl = $return_url;
  }

  /**
   * Fake date.
   *
   * @return false|string
   *   Fake date.
   */
  public function getDate() {
    return $this->fakeDate;
  }

  /**
   * Fake order info.
   *
   * @return string
   *   Fake order info.
   */
  public function getOrderInfo() {
    return $this->fakeInfo;
  }

  /**
   * Fake ip.
   *
   * @return string|null
   *   Fake ip.
   */
  public function getIpAddress() {
    return $this->fakeIp;
  }

  /**
   * Fake return url.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   Fake return url.
   */
  public function getReturnUrl() {
    return $this->fakeReturnUrl;
  }

}
