<?php

namespace Drupal\Tests\commerce_vnpay\Unit;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_vnpay\VNPAY\TransactionResponse;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaymentGatewayTest.
 *
 * @package Drupal\Tests\commerce_vnpay\Unit
 */
class PaymentGatewayTest extends UnitTestCase {

  /**
   * Input array.
   *
   * @var array
   */
  protected $inputArray;

  /**
   * Drupal\Tests\commerce_vnpay\Unit\PaymentGateway.
   *
   * @var \Drupal\Tests\commerce_vnpay\Unit\PaymentGateway
   */
  protected $paymentGateWay;

  /**
   * Drupal\commerce_payment\Entity\PaymentInterface.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $paymentEntity;

  protected $data;

  /**
   * Drupal\commerce_order\Entity\OrderInterface.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $orderEntity;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->inputArray = [
      'test1' => 'value1',
      'test2' => 'value2',
      'test3' => 'value3',
      'test4' => 1,
    ];

    $requestStack = $this->getMockBuilder(RequestStack::class)->getMock();

    $this->paymentGateWay = new PaymentGateway($requestStack);

    $this->data['amount'] = new Price('10000', 'VND');

    $this->orderEntity = $this->getMockBuilder(OrderInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->orderEntity->expects($this->any())
      ->method('id')
      ->willReturn($this->randomMachineName());

    $this->paymentEntity = $this->getMockBuilder(PaymentInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->paymentEntity->expects($this->any())
      ->method('id')
      ->willReturn($this->randomMachineName());
    $this->paymentEntity->expects($this->any())
      ->method('getAmount')
      ->willReturn($this->data['amount']);
    $this->paymentEntity->expects($this->any())
      ->method('getOrder')
      ->willReturn($this->orderEntity);

    $this->paymentGateWay->setEntity($this->paymentEntity);
  }

  /**
   * Test hash function.
   */
  public function testHashFunction() {
    $string_to_hash = 'aaa';
    $key = 'key';
    $sha = 'sha256';
    $pay_gate = $this->paymentGateWay;
    $this->assertTrue(is_string($pay_gate->hash($string_to_hash, $key, $sha)));
    $this->assertEquals('7e10efb62e4acbe310f8091d536c0d6189402fc38d3ff4d6d02274ae1f7a413e', $pay_gate->hash($string_to_hash, $key, $sha));
  }

  /**
   * Test generate string to hash from a array.
   */
  public function testGenerateStringToHash() {
    $pay_gate = $this->paymentGateWay;
    $this->assertTrue(is_string($pay_gate->generateStringToHash($this->inputArray)));

    $expected = 'test1=value1&test2=value2&test3=value3&test4=1';
    $this->assertEquals($expected, $pay_gate->generateStringToHash($this->inputArray));
  }

  /**
   * Test hash and generate string.
   */
  public function testHashFromGenerateString() {
    $key = 'key';
    $sha = 'sha256';
    $pay_gate = $this->paymentGateWay;
    $string_to_hash = $pay_gate->generateStringToHash($this->inputArray);
    $expected = hash($sha, $key . $string_to_hash);
    $this->assertEquals($expected, $pay_gate->hash($string_to_hash, $key, $sha));
  }

  /**
   * Test generate url query.
   */
  public function testGenerateUrlQuery() {
    $input = $this->inputArray;
    $output = $this->paymentGateWay->generateUrlQuery($input);
    $this->assertTrue(is_string($output));

    $input['test5'] = 'http://test.com';
    $expected = 'test1=' . urlencode($input['test1']) . '&test2=' . urlencode($input['test2']) . '&test3=' . urlencode($input['test3']) . '&test4=' . urlencode($input['test4']);
    $expected .= '&test5=' . urlencode($input['test5']);
    $output = $this->paymentGateWay->generateUrlQuery($input);
    $this->assertEquals($expected, $output);
  }

  /**
   * Test mock payment amount.
   */
  public function testGetPaymentAmount() {
    $this->assertEquals($this->data['amount'], $this->paymentGateWay->getAmount());
  }

  /**
   * Test redirect url.
   */
  public function testRedirectUrl() {
    $amount = $this->data['amount'];
    $configuration = [
      'paygate_url' => 'http://sandbox.vnpayment.vn/paymentv2/vpcpay.htm',
      'merchant_code' => 'Y875MTH9',
      'hash_secret' => 'LZHDMKFGPJOFEQMDBBULVUKACCWTMXEO',
    ];
    $this->paymentGateWay->setConfiguration($configuration);
    $date = $this->getRandomGenerator()->string();
    $ip = $this->getRandomGenerator()->string();
    $info = $this->getRandomGenerator()->string();
    $return_url = $this->getRandomGenerator()->string();
    $this->paymentGateWay->setFakeDate($date);
    $this->paymentGateWay->setFakeIp($ip);
    $this->paymentGateWay->setFakeInfo($info);
    $this->paymentGateWay->setFakeReturnUrl($return_url);
    $expected = [
      "vnp_Version" => "2.0.0",
      "vnp_TmnCode" => $configuration['merchant_code'],
      "vnp_Amount" => $amount->getNumber() * 100,
      "vnp_Command" => "pay",
      "vnp_CreateDate" => $date,
      "vnp_CurrCode" => "VND",
      "vnp_IpAddr" => $ip,
      "vnp_Locale" => 'vn',
      "vnp_OrderInfo" => $info,
      "vnp_ReturnUrl" => $return_url,
      "vnp_TxnRef" => $this->paymentEntity->id() . '_' . $date,
    ];
    ksort($expected);
    $this->assertEquals($expected, $this->paymentGateWay->getRedirectArguments());
    $string_to_hash = $this->paymentGateWay->generateStringToHash($expected);
    $hashed_string = $this->paymentGateWay->hash($string_to_hash, $configuration['hash_secret'], 'sha256');
    $url_query = $this->paymentGateWay->generateUrlQuery($expected);
    $vnp_Url = $configuration['paygate_url'] . "?" . $url_query;
    $vnp_Url .= '&vnp_SecureHashType=SHA256&vnp_SecureHash=' . $hashed_string;

    $this->assertEquals($vnp_Url, $this->paymentGateWay->generateRedirectUrl());
  }

  /**
   * Test order id from payment object.
   */
  public function testOrderId() {
    $order = $this->paymentGateWay->getOrder();
    $this->assertNotEmpty($order);

    $this->assertEquals($this->orderEntity->id(), $order->id());

  }

  /**
   * Test get response.
   */
  public function testGetResponse() {
    $configuration = [
      'paygate_url' => 'http://sandbox.vnpayment.vn/paymentv2/vpcpay.htm',
      'merchant_code' => 'Y875MTH9',
      'hash_secret' => 'LZHDMKFGPJOFEQMDBBULVUKACCWTMXEO',
    ];
    $this->paymentGateWay->setConfiguration($configuration);

    $data = [
      'vnp_TmnCode' => $this->randomMachineName(),
      'vnp_Amount' => $this->randomMachineName(),
      'vnp_BankCode' => $this->randomMachineName(),
      'vnp_BankTranNo' => $this->randomMachineName(),
      'vnp_CardType' => $this->randomMachineName(),
      'vnp_PayDate' => $this->randomMachineName(),
      'vnp_CurrCode' => $this->randomMachineName(),
      'vnp_OrderInfo' => $this->randomMachineName(),
      'vnp_TransactionNo' => $this->randomMachineName(),
      'vnp_ResponseCode' => '00',
      'vnp_TxnRef' => $this->paymentEntity->id() . '_' . date('ymdHsi'),
    ];
    ksort($data);
    $string_to_hash = $this->paymentGateWay->generateStringToHash($data);
    $hash = $this->paymentGateWay->hash($string_to_hash, $configuration['hash_secret']);
    $data['vnp_SecureHashType'] = 'SHA256';
    $data['vnp_SecureHash'] = $hash;

    $response = $this->paymentGateWay->getResponse($data);
    $this->assertEquals(TransactionResponse::SUCCESS, $response->getCode());

    $data['vnp_ResponseCode'] = '01';
    $response = $this->paymentGateWay->getResponse($data);
    $this->assertEquals(TransactionResponse::TRANS_EXIST, $response->getCode());

    $response = $this->paymentGateWay->getResponse($data);
    $this->assertEquals($this->paymentEntity->id(), $response->getTxnRef());
  }

  /**
   * Test verify hash.
   */
  public function testVerifyHash() {
    $configuration = [
      'paygate_url' => 'http://sandbox.vnpayment.vn/paymentv2/vpcpay.htm',
      'merchant_code' => 'Y875MTH9',
      'hash_secret' => 'LZHDMKFGPJOFEQMDBBULVUKACCWTMXEO',
    ];
    $this->paymentGateWay->setConfiguration($configuration);

    $data = [
      'vnp_TmnCode' => $this->randomMachineName(),
      'vnp_Amount' => $this->randomMachineName(),
      'vnp_BankCode' => $this->randomMachineName(),
      'vnp_BankTranNo' => $this->randomMachineName(),
      'vnp_CardType' => $this->randomMachineName(),
      'vnp_PayDate' => $this->randomMachineName(),
      'vnp_CurrCode' => $this->randomMachineName(),
      'vnp_OrderInfo' => $this->randomMachineName(),
      'vnp_TransactionNo' => $this->randomMachineName(),
      'vnp_ResponseCode' => '00',
      'vnp_TxnRef' => $this->paymentEntity->id() . '_' . date('ymdHsi'),
    ];
    ksort($data);
    $string_to_hash = $this->paymentGateWay->generateStringToHash($data);
    $hash = $this->paymentGateWay->hash($string_to_hash, $configuration['hash_secret']);
    $data['vnp_SecureHashType'] = 'SHA256';
    $data['vnp_SecureHash'] = $hash;

    $this->assertEquals(TRUE, $this->paymentGateWay->verifySecureHash($data));

    $data['vnp_SecureHash'] = 'aaaa';
    $this->assertEquals(FALSE, $this->paymentGateWay->verifySecureHash($data));
  }

}
