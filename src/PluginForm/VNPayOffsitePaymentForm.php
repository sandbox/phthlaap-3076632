<?php

namespace Drupal\commerce_vnpay\PluginForm;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_vnpay\VNPAY\PaymentGateway;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VNPayOffsitePaymentForm.
 *
 * @package Drupal\commerce_vnpay\PluginForm
 */
class VNPayOffsitePaymentForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  protected $paymentGateway;

  protected $logger;

  /**
   * VNPayOffsitePaymentForm constructor.
   *
   * @param \Drupal\commerce_vnpay\VNPAY\PaymentGateway $paymentGateway
   *   Payment gateway manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   LoggerChannelInterface interface.
   */
  public function __construct(PaymentGateway $paymentGateway, LoggerChannelInterface $logger) {
    $this->paymentGateway = $paymentGateway;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('commerce_vnpay.payment_gateway'), $container->get('logger.channel.commerce_momo'));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $payment = $this->entity;
    try {
      $payment->save();
      $this->paymentGateway->setEntity($payment);
    }
    catch (EntityStorageException $e) {
      $this->logger->error($e->getMessage());
      return new NeedsRedirectException($this->getReviewUrl());
    }
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $this->paymentGateway->setConfiguration($payment_gateway_plugin->getConfiguration());
    $form = $this->buildRedirectForm($form, $form_state, $this->paymentGateway->generateRedirectUrl(), [], 'get');
    return $form;
  }

  /**
   * Get order review url.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   Url.
   */
  public function getReviewUrl() {
    return Url::FromRoute('commerce_checkout.form', [
      'absolute' => TRUE,
      'step' => 'review',
      'commerce_order' => $this->entity->getOrderId(),
    ])->toString();
  }

}
