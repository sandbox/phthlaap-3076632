<?php

namespace Drupal\commerce_vnpay\VNPAY;

/**
 * Class GatewayArgument.
 *
 * @package Drupal\commerce_vnpay\VNPAY
 */
class GatewayArguments {

  const RESPONSE_CODE = 'vnp_ResponseCode';

  const VERSION = "vnp_Version";

  const TMN_CODE = "vnp_TmnCode";

  const AMOUNT = "vnp_Amount";

  const COMMAND = "vnp_Command";

  const CREATE_DATE = "vnp_CreateDate";

  const CURR_CODE = "vnp_CurrCode";

  const IP_ADDRESS = "vnp_IpAddr";

  const LOCALE = "vnp_Locale";

  const ORDER_INFO = "vnp_OrderInfo";

  const RETURN_URL = "vnp_ReturnUrl";

  const TXN_REF = "vnp_TxnRef";

  const HASH_TYPE = "vnp_SecureHashType";

  const SECURE_HASH = "vnp_SecureHash";

  const TRANSACTION_NO = "vnp_TransactionNo";

}
