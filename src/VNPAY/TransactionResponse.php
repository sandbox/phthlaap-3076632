<?php

namespace Drupal\commerce_vnpay\VNPAY;

/**
 * Class Response.
 *
 * @see http://sandbox.vnpayment.vn/apis/docs/bang-ma-loi/
 *
 * @package Drupal\commerce_vnpay\VNPAY
 */
class TransactionResponse {

  const SUCCESS = '00';

  const TRANS_EXIST = '01';

  const INVALID_MERCHANT = '02';

  const INVALID_FORMAT = '03';

  const WEBSITE_LOCKED = '04';

  const FAIL_PASSWORD_TIMES = '05';

  const INVALID_OPT = '13';

  const SUSPECTED_FRAUD = '07';

  const FAIL_INTERNET_BANKING_REGISTRATION = '09';

  const CARD_VERIFY_FAIL = '10';

  const TRANS_EXPIRE = '11';

  const CARD_LOCKED = '12';

  const INSUFFICIENT_ACCOUNT_BALANCE = '51';

  const DAILY_TRANS_LIMIT = '65';

  const BANK_SYSTEM_MAINTAINING = '08';

  const UNKNOWN_ERROR = '99';

  const INVALID_SECURE_HASH = '97';

  const MESSAGES = [
    TransactionResponse::SUCCESS => 'Giao dịch thành công',
    TransactionResponse::TRANS_EXIST => 'Giao dịch đã tồn tại',
    TransactionResponse::INVALID_MERCHANT => 'Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)',
    TransactionResponse::INVALID_FORMAT => 'Dữ liệu gửi sang không đúng định dạng',
    TransactionResponse::WEBSITE_LOCKED => 'Khởi tạo GD không thành công do Website đang bị tạm khóa',
    TransactionResponse::FAIL_PASSWORD_TIMES => 'Giao dịch không thành công do: Quý khách nhập sai mật khẩu quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch',
    TransactionResponse::INVALID_OPT => 'Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.',
    TransactionResponse::SUSPECTED_FRAUD => 'Giao dịch bị nghi ngờ là giao dịch gian lận.',
    TransactionResponse::FAIL_INTERNET_BANKING_REGISTRATION => 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.',
    TransactionResponse::CARD_VERIFY_FAIL => 'Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần',
    TransactionResponse::TRANS_EXPIRE => 'Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.',
    TransactionResponse::CARD_LOCKED => 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.',
    TransactionResponse::INSUFFICIENT_ACCOUNT_BALANCE => 'Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.',
    TransactionResponse::DAILY_TRANS_LIMIT => 'Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.',
    TransactionResponse::BANK_SYSTEM_MAINTAINING => 'Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì. Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này.',
    TransactionResponse::UNKNOWN_ERROR => 'Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê).',
    TransactionResponse::INVALID_SECURE_HASH => 'Chữ ký không hợp lệ.',
  ];

  /**
   * Response code.
   *
   * @var string
   */
  protected $code;

  /**
   * Message.
   *
   * @var string
   */
  protected $message;

  /**
   * Transaction reference id.
   *
   * @var string
   */
  protected $txnRef;

  /**
   * Transaction number.
   *
   * @var string
   */
  protected $transactionNo;

  /**
   * TransactionResponse constructor.
   *
   * @param array $response_data
   *   Response data from VNPAY.
   */
  public function __construct(array $response_data) {
    $this->code = $response_data[GatewayArguments::RESPONSE_CODE];
    if (!empty($response_data[GatewayArguments::TXN_REF])) {
      $txn_explode = explode('_', $response_data[GatewayArguments::TXN_REF]);
      $this->txnRef = $txn_explode[0];
    }
    if (!empty($response_data[GatewayArguments::TRANSACTION_NO])) {
      $this->transactionNo = $response_data[GatewayArguments::TRANSACTION_NO];
    }
  }

  /**
   * Get response code.
   *
   * @return string
   *   Return response code.
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Get message.
   *
   * @return string
   *   Return message text.
   */
  public function getMessage() {
    if (empty(TransactionResponse::MESSAGES[$this->code])) {
      return TransactionResponse::MESSAGES[TransactionResponse::UNKNOWN_ERROR];
    }
    return TransactionResponse::MESSAGES[$this->code];
  }

  /**
   * Get Transaction reference.
   *
   * @return string
   *   Transaction reference.
   */
  public function getTxnRef() {
    return $this->txnRef;
  }

  /**
   * Get Transaction number.
   *
   * @return string
   *   Transaction number.
   */
  public function getTransactionNo() {
    return $this->transactionNo;
  }

}
