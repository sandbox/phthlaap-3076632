<?php

namespace Drupal\commerce_vnpay\VNPAY;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaymentGateway.
 *
 * @package Drupal\commerce_vnpay\VNPAY
 */
class PaymentGateway {

  protected $hashSecret;

  protected $payGateURL;

  protected $merchantCode;

  /**
   * Drupal\commerce_payment\Entity\PaymentInterface.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * Drupal\commerce_order\Entity\OrderInterface.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * PaymentGateway constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   RequestStack.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * Set configuration.
   *
   * @param array $configuration
   *   Plugin configuration.
   */
  public function setConfiguration(array $configuration) {
    $this->payGateURL = $configuration['paygate_url'];
    $this->merchantCode = $configuration['merchant_code'];
    $this->hashSecret = $configuration['hash_secret'];
  }

  /**
   * Get argument to build hash.
   *
   * @return array
   *   Array argument.
   */
  public function getRedirectArguments() {
    $amount = $this->payment->getAmount();
    $date = $this->getDate();
    return [
      GatewayArguments::VERSION => "2.0.0",
      GatewayArguments::TMN_CODE => $this->merchantCode,
      GatewayArguments::AMOUNT => $amount->getNumber() * 100,
      GatewayArguments::COMMAND => "pay",
      GatewayArguments::CREATE_DATE => $date,
      GatewayArguments::CURR_CODE => "VND",
      GatewayArguments::IP_ADDRESS => $this->getIpAddress(),
      GatewayArguments::LOCALE => 'vn',
      GatewayArguments::ORDER_INFO => $this->getOrderInfo(),
      GatewayArguments::RETURN_URL => $this->getReturnUrl(),
      GatewayArguments::TXN_REF => $this->payment->id() . '_' . $date,
    ];
  }

  /**
   * Generate redirect url.
   *
   * @see https://sandbox.vnpayment.vn/apis/docs/huong-dan-tich-hop/
   *
   * @return string
   *   Redirect url.
   */
  public function generateRedirectUrl() {
    $arguments = $this->getRedirectArguments();
    ksort($arguments);
    $string_to_hash = $this->generateStringToHash($arguments);
    $hash_string = $this->hash($string_to_hash, $this->hashSecret, 'sha256');
    $url_query = $this->generateUrlQuery($arguments);
    return $this->payGateURL . '?' . $url_query . '&vnp_SecureHashType=SHA256&vnp_SecureHash=' . $hash_string;
  }

  /**
   * Hash a string with key.
   *
   * @param string $string_to_hash
   *   String to hash.
   * @param string $key
   *   Key.
   * @param string $sha
   *   Hash type.
   *
   * @return string
   *   Hash string.
   */
  public function hash($string_to_hash, $key, $sha = 'sha256') {
    return hash($sha, $key . $string_to_hash);
  }

  /**
   * Generate a string before hash by array input.
   *
   * @param array $input
   *   Array key => value to hash.
   *
   * @return string
   *   Output string.
   */
  public function generateStringToHash(array $input) {
    $output = '';
    foreach ($input as $key => $value) {
      $output .= (empty($output) ? '' : '&') . $key . '=' . $value;
    }
    return $output;
  }

  /**
   * Generate url query from array input.
   *
   * @param array $input
   *   Array key => value to hash.
   *
   * @return string
   *   Output string.
   */
  public function generateUrlQuery(array $input) {
    $output = '';
    foreach ($input as $key => $value) {
      $output .= (empty($output) ? '' : '&') . urlencode($key) . '=' . urlencode($value);
    }
    return $output;
  }

  /**
   * Set payment entity to service.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $paymentEntity
   *   PaymentInterface.
   */
  public function setEntity(PaymentInterface $paymentEntity) {
    $this->payment = $paymentEntity;
    $this->order = $paymentEntity->getOrder();
  }

  /**
   * Get payment amount.
   *
   * @return \Drupal\commerce_price\Price|null
   *   Price.
   */
  public function getAmount() {
    return $this->payment->getAmount();
  }

  /**
   * Get date with format, this function for testing purpose.
   *
   * @return false|string
   *   Date string.
   */
  public function getDate() {
    return date('YmdHis');
  }

  /**
   * Get client ip.
   *
   * @return string|null
   *   Client ip.
   */
  protected function getIpAddress() {
    return $this->requestStack->getCurrentRequest()->getClientIp();
  }

  /**
   * Get order info by order item title.
   *
   * @return string
   *   Order info.
   */
  protected function getOrderInfo() {
    $order_info = [];
    foreach ($this->order->getItems() as $order_item) {
      $order_info[] = $order_item->getTitle();
    }
    return implode('; ', $order_info);
  }

  /**
   * Get return url of payment process.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   Return url.
   */
  protected function getReturnUrl() {
    return Url::FromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
  }

  /**
   * Return order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   Order object.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Get response object.
   *
   * @param array $data
   *   Data redirect form gateway.
   *
   * @return \Drupal\commerce_vnpay\VNPAY\TransactionResponse
   *   Response object.
   */
  public function getResponse(array $data) {
    if (empty($data[GatewayArguments::RESPONSE_CODE])) {
      throw new PaymentGatewayException('Response code can not empty.');
    }
    return new TransactionResponse($data);
  }

  /**
   * Verify secure hash from gateway.
   *
   * @param array $data
   *   Data from gateway.
   *
   * @return bool
   *   Verify result.
   */
  public function verifySecureHash(array $data) {
    if (empty($data[GatewayArguments::SECURE_HASH])) {
      throw new PaymentGatewayException('Secure hash can not empty.');
    }
    if (empty($this->hashSecret)) {
      throw new PaymentGatewayException('Secret can not empty.');
    }
    $hash_to_verify = $data[GatewayArguments::SECURE_HASH];
    unset($data[GatewayArguments::SECURE_HASH]);
    unset($data[GatewayArguments::HASH_TYPE]);
    ksort($data);
    $string_to_hash = $this->generateStringToHash($data);
    $hash = $this->hash($string_to_hash, $this->hashSecret);
    return ($hash === $hash_to_verify);
  }

}
