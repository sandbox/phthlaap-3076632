<?php

namespace Drupal\commerce_vnpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_vnpay\VNPAY\PaymentGateway;
use Drupal\commerce_vnpay\VNPAY\TransactionResponse;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provide payment gateway plugin for VNPAY.
 *
 * @CommercePaymentGateway(
 *   id = "vnpay_payment_gateway",
 *   label = @Translation("VNPay payment gateway"),
 *   display_label = @Translation("VNPay payment gateway"),
 *   forms = {
 *      "offsite-payment" =
 *   "Drupal\commerce_vnpay\PluginForm\VNPayOffsitePaymentForm",
 *   },
 *   payment_type = "vnpay_payment"
 * )
 */
class VNPayOffsitePaymentGateway extends OffsitePaymentGatewayBase {

  /**
   * Drupal\commerce_vnpay\VNPAY\PaymentGateway.
   *
   * @var \Drupal\commerce_vnpay\VNPAY\PaymentGateway
   */
  protected $paymentGateway;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              PaymentTypeManager $payment_type_manager,
                              PaymentMethodTypeManager $payment_method_type_manager,
                              TimeInterface $time,
                              PaymentGateway $paymentGateway,
                              LoggerChannelInterface $loggerChannel,
                              MessengerInterface $messenger) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );
    $this->paymentGateway = $paymentGateway;
    $this->loggerChannel = $loggerChannel;
    $this->messenger = $messenger;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_vnpay.payment_gateway'),
      $container->get('logger.channel.commerce_vnpay'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = [
      'paygate_url' => '',
      'merchant_code' => '',
      'hash_secret' => '',
    ];
    return $configuration + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['merchant_code'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Merchant Code'),
      '#default_value' => $this->configuration['merchant_code'] ? $this->configuration['merchant_code'] : '',
    ];
    $form['hash_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Secret'),
      '#default_value' => $this->configuration['hash_secret'] ? $this->configuration['hash_secret'] : '',
      '#required' => TRUE,
    ];
    $form['paygate_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Paygate Url'),
      '#default_value' => $this->configuration['paygate_url'] ? $this->configuration['paygate_url'] : '',
      '#required' => TRUE,
      '#description' => t('Change to Dev (Test) / Production (Live) API Endpoint as per mode selected above.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_code'] = $values['merchant_code'];
      $this->configuration['hash_secret'] = $values['hash_secret'];
      $this->configuration['paygate_url'] = $values['paygate_url'];
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $transaction_response = $this->paymentGateway->getResponse($request->query->all());
    if (TransactionResponse::SUCCESS != $transaction_response->getCode()) {
      $this->messenger->addError($transaction_response->getMessage());
      throw new PaymentGatewayException($transaction_response->getMessage());
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->load($transaction_response->getTxnRef());
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $this->paymentGateway->setEntity($payment);
    $this->paymentGateway->setConfiguration($payment_gateway_plugin->getConfiguration());

    if (FALSE == $this->paymentGateway->verifySecureHash($request->query->all())) {
      $this->messenger->addError($this->t('Invalid secure hash.'));
      throw new PaymentGatewayException('Invalid secure hash.');
    }

    $payment->set('remote_id', $transaction_response->getTransactionNo());
    $payment->set('state', 'authorization');
    $payment->set('authorized', $this->time->getRequestTime());
    try {
      $payment->save();
      $this->loggerChannel->log('notice', 'Payment entity with id ' . $payment->id() . ' has been updated.');
      $this->messenger->addMessage($this->t('Your payment has been completed.'));
    }
    catch (\Exception $ex) {
      $this->messenger->addError($this->t('Cannot update payment.'));
      throw new PaymentGatewayException($this->t('Cannot update payment.'));
    }
  }

}
