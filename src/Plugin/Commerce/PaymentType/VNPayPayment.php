<?php

namespace Drupal\commerce_vnpay\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provide payment type VNPay.
 *
 * @CommercePaymentType(
 *   id = "vnpay_payment",
 *   label = @Translation("VNPay"),
 * )
 */
class VNPayPayment extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
